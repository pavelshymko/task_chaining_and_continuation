using NUnit.Framework;


namespace TaskChainingAndContinuation.Test
{
    public class TaskChainingAndContinuationTests
    {
        int[] array = {11, 2, 34, 48, 5, 66, 79, 89, 900, 600};
        
        [Test]
        public void CreateArrayShouldReturnArrayOf10RandomIntegers()
        {
            var arr = TaskContinuation.CreateArray();
            var actual = arr.Length;
            var expected = 10;
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void MultipliesArrayShouldReturnArrayMultipliesByNumber()
        {
            
            var actual = TaskContinuation.MultipliesArray(array, 10);
            int[] expected = {110, 20, 340, 480, 50, 660, 790, 890, 9000, 6000};
            
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void SortArrayShouldReturnSortArrayByAscending()
        {
            var actual = TaskContinuation.SortArray(array) ;
            Array.Sort(array);
            var expected = array;
            Assert.That(actual, Is.EqualTo(expected));
        }
        
        [Test]
        public void CalculateAverageShouldReturnAverageValue()
        {
            var actual = TaskContinuation.CalculateAverage(array);
            var expected = array.Average();
            
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
    
}



