﻿namespace TaskChainingAndContinuation
{
    class Program
    {
        public static void Main()
        {
            double averageValue = TaskContinuation.GetAverageValue();
            Console.WriteLine(averageValue);
        }
    }
    
    
    public class TaskContinuation {
 
        public static double GetAverageValue()
        {
            Task<int[]> createArrayTask = Task.Run(CreateArray);
            Task<int[]> multiplyArrayTask = createArrayTask
                .ContinueWith(ant => MultipliesArray(ant.Result, 10));
            Task<int[]> sortArrayTask = multiplyArrayTask
                .ContinueWith(ant => SortArray(ant.Result));
            Task<double> averageValueTask = sortArrayTask
                .ContinueWith(ant => CalculateAverage(ant.Result));

            return averageValueTask.Result;
        }

        public static int[] CreateArray() {
            
            Random randNum = new Random();
            int[] array = Enumerable
                .Repeat(0, 10)
                .Select(i => randNum.Next(0, 10))
                .ToArray();

            Console.WriteLine("Create an array of 10 random integers:");
            foreach (var number in array)
            {
                Console.Write($"{number} ");
            }
            Console.WriteLine();
            
            return array;
        }
        
        public static int[] MultipliesArray(int[] array, int randomNumber) 
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] *= randomNumber;
            }
                
            Console.WriteLine($"Multiplies array by {randomNumber} number:");
            foreach (var num in array)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
                
            return array;
        }
        
        public static int[] SortArray(int[] array) 
        {
            Array.Sort(array);
                
            Console.WriteLine("Sorting array:");
            foreach (var num in array)
            {
                Console.Write($"{num} ");
            }
            Console.WriteLine();
                
            return array;
        }
        
        public static double CalculateAverage(int[] array)
        {
            double averageValue = array.Average();
            Console.WriteLine($"Average value: {averageValue}");
            return averageValue;
        }
    }
}

